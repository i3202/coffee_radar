import os
import configparser


class Config:

    def __init__(self):
        self.config = configparser.ConfigParser()
        file_config = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(file_config)

    def get_val(self, key: str):
        return self.config['config'][key]
