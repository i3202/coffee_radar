import os
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import Dispatcher

from src.utils.db_coffee_shops import create_db_backup, write_new_shop, get_elements, delete_coffee_point, check_coffee_point_id
from src.utils.db_user_info import get_users
from src.utils.translations import Translations
from src.utils.buttons import Buttons
from src.config import Config

config = Config()
id_admin = config.get_val('id_admin')
NAME_DB_BACKUP = config.get_val('name_db_backup')


class DelPoint(StatesGroup):
    del_point = State()


class AddPoint(StatesGroup):
    add_new_point_title = State()
    add_new_point_inst = State()
    add_new_point_coordinates = State()
    add_new_point_work_schedule = State()
    add_new_point_address = State()


class Admin:

    def __init__(self):
        self.buttons = Buttons()
        self.text = Translations()

    async def make_backup(self, bot):
        await create_db_backup()
        path = os.path.join(os.getcwd(), 'data', 'db_shops')
        file = f'{path}/{NAME_DB_BACKUP}'
        check_file = os.path.exists(file)
        try:
            if check_file:
                await bot.send_document(chat_id=id_admin, document=open(file, "rb"))
            else:
                await bot.send_message(chat_id=id_admin, text='Его нет')
        except Exception as ex:
            print(ex)

    def register_handlers_admin(self, dp: Dispatcher):
        dp.register_message_handler(self.add_new_point_title, state=AddPoint.add_new_point_title)
        dp.register_message_handler(self.add_new_point_inst, state=AddPoint.add_new_point_inst)
        dp.register_message_handler(self.add_new_point_coordinates, content_types=['location', 'text'], state=AddPoint.add_new_point_coordinates)
        dp.register_message_handler(self.add_new_point_work_schedule, state=AddPoint.add_new_point_work_schedule)
        dp.register_message_handler(self.add_new_point_address, state=AddPoint.add_new_point_address)
        dp.register_message_handler(self.delete_point, state=DelPoint.del_point)

    async def on_main(self, message, state: FSMContext):
        await state.finish()
        markup = self.buttons.send_location()
        return await message.answer(text=self.text.start, reply_markup=markup)

    async def write_state(self, state: FSMContext, key, value):
        async with state.proxy() as data:
            data[key] = value

    async def read_state(self, state: FSMContext):
        async with state.proxy() as data:
            return data

    async def add_new_point_title(self, message, state: FSMContext):
        text = message.text
        match str(text).lower():
            case 'назад':
                await state.finish()
                return await message.answer(text=self.text.welcome_admin, reply_markup=self.buttons.admin_btn())
            case 'на главную':
                return await self.on_main(message, state)

        await self.write_state(state, 'title', str(text))
        await message.answer(text=self.text.add_new_point_inst, reply_markup=self.buttons.back())
        await AddPoint.add_new_point_inst.set()

    async def add_new_point_inst(self, message, state: FSMContext):
        text = message.text
        match str(text).lower():
            case 'назад':
                await message.answer(text=self.text.add_new_point_title, reply_markup=self.buttons.back())
                return await AddPoint.add_new_point_title.set()
            case 'на главную':
                return await self.on_main(message, state)

        if str(text).lower() in ['null', 'false', '0', 'none']:
            text = None

        await self.write_state(state, 'url_inst', text)
        await message.answer(text=self.text.add_new_point_coordinates, reply_markup=self.buttons.back())
        await AddPoint.add_new_point_coordinates.set()

    async def add_new_point_coordinates(self, message, state: FSMContext):
        text = message.text
        match str(text).lower():
            case 'назад':
                await message.answer(text=self.text.add_new_point_inst, reply_markup=self.buttons.back())
                return await AddPoint.add_new_point_inst.set()
            case 'на главную':
                return await self.on_main(message, state)

        location = message.location
        if location:
            latitude = location.latitude
            longitude = location.longitude
        else:
            try:
                latitude, longitude = message.text.split(',')
                latitude = float(latitude)
                longitude = float(longitude)
            except Exception as ex:
                print('ERROR:', ex)
                await message.answer(text=self.text.add_new_point_coordinates, reply_markup=self.buttons.back())
                return await AddPoint.add_new_point_coordinates.set()

        await self.write_state(state, 'latitude', latitude)
        await self.write_state(state, 'longitude', longitude)
        await message.answer(text=self.text.add_new_point_work_schedule, reply_markup=self.buttons.back())
        await AddPoint.add_new_point_work_schedule.set()

    async def add_new_point_work_schedule(self, message, state: FSMContext):
        text = str(message.text).upper()
        match str(text).lower():
            case 'назад':
                await message.answer(text=self.text.add_new_point_coordinates, reply_markup=self.buttons.back())
                return await AddPoint.add_new_point_coordinates.set()
            case 'на главную':
                return await self.on_main(message, state)

        if str(text).lower() in ['null', 'false', '0', 'none']:
            text = None

        await self.write_state(state, 'time_work', text)
        await message.answer(text=self.text.add_new_point_address, reply_markup=self.buttons.back())
        await AddPoint.add_new_point_address.set()

    async def add_new_point_address(self, message, state: FSMContext):
        text = message.text
        match str(text).lower():
            case 'назад':
                await message.answer(text=self.text.add_new_point_work_schedule, reply_markup=self.buttons.back())
                return await AddPoint.add_new_point_work_schedule.set()
            case 'на главную':
                return await self.on_main(message, state)

        await self.write_state(state, 'address', text)
        await self.write_shop(message, state)

    async def write_shop(self, message, state: FSMContext):
        state_write_shop = await self.read_state(state)
        title = state_write_shop.get('title')
        inst_url = state_write_shop.get('url_inst')
        latitude = state_write_shop.get('latitude')
        longitude = state_write_shop.get('longitude')
        time_work = state_write_shop.get('time_work')
        address = state_write_shop.get('address')
        await write_new_shop(title, inst_url, latitude, longitude, time_work, address)
        await message.answer(text='Кофейня добавлена ✅', reply_markup=self.buttons.send_location())
        await state.finish()

    async def get_statistics(self, message):
        get_el = await get_elements()
        get_u = await get_users()
        text = f'Кофеен: {len(get_el)} шт.\n' \
               f'Пользователи: {len(get_u)} чел.'
        await message.answer(text=text)

    async def delete_point(self, message, state: FSMContext):
        text = message.text
        check_point = await check_coffee_point_id(text)

        match str(text).lower():
            case 'на главную':
                return await self.on_main(message, state)

        if check_point:
            name = check_point[1]
            address = check_point[6]
            await delete_coffee_point(text)
            await state.finish()
            await message.answer(text=f'Кофейня {name} по адресу {address} удалена ✅',
                                 reply_markup=self.buttons.send_location())
        else:
            await message.answer(text='Такой кофейни нет')
