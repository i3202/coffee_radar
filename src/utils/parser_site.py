import os
import json
import aiohttp
import asyncio
from datetime import datetime
from user_agent import generate_user_agent
from fp.fp import FreeProxy
from bs4 import BeautifulSoup as bs
from slimit import ast
from slimit.parser import Parser
from slimit.visitors import nodevisitor
from threading import Thread
from src.utils.db_coffee_shops import write_new_shop, check_coffee_point, create_db_backup
from src.config import Config

config = Config()
url_site = config.get_val('url_site')


class ParsingSite:

    def __init__(self):
        Thread(target=self._main, daemon=True).start()

    async def _add_more_db(self):
        outpath = os.path.join(os.getcwd(), 'data')
        inpath_file = os.path.join(outpath, 'my_coffee_shops.json')
        with open(inpath_file) as json_file:
            data = json.load(json_file)
            for d in data:
                name = d.get('name')
                inst = d.get('instagram')
                place = d.get('places')
                for p in place:
                    pl = place.get(p)
                    address = pl.get('address')
                    time_work = pl.get('time_work')
                    lat = pl.get('lat')
                    lng = pl.get('lng')

                    check_point = await check_coffee_point(lat, lng)
                    if check_point is False:
                        await write_new_shop(name, inst, lat, lng, time_work, address)

    async def _places_info(self, soup):
        places = {}
        all_scripts = soup.find_all('script')
        addrs = None
        time_work = None
        lat = None
        lng = None
        id_p = None

        parser = Parser()
        tree = parser.parse(all_scripts[3].text)
        for node in nodevisitor.visit(tree):
            if isinstance(node, ast.Assign):
                a = getattr(node.left, 'value', '')
                b = getattr(node.right, 'value', '')
                if 'address' == a and b != '' and b is not None:
                    addrs = ", ".join(b.replace('"', '').split(',')[:2])
                if 'time' == a and b != '' and b is not None:
                    time_work = b.replace('"', '')
                if 'lat' == a and b != '' and b is not None:
                    lat = b
                if 'lng' == a and b != '' and b is not None:
                    lng = b
                if 'id' == a and b != '' and b is not None:
                    id_p = b
                if id_p is not None:
                    places.update({
                        id_p: {
                            'address': addrs,
                            'time_work': time_work,
                            'lat': lat,
                            'lng': lng,
                        },
                    })
        return places

    async def _write_to_db(self, list_coffee_shops):
        for coffee_shop in list_coffee_shops:
            key = list(coffee_shop.keys())[0]
            name = coffee_shop.get(key).get('name')
            inst = coffee_shop.get(key).get('instagram')
            places = coffee_shop.get(key).get('places')

            for key in places:
                place = places.get(key)
                address = place.get('address')
                time_work = place.get('time_work')
                lat = place.get('lat')
                lng = place.get('lng')
                check_point = await check_coffee_point(lat, lng)

                if check_point is False:
                    await write_new_shop(name, inst, lat, lng, time_work, address)

    async def _page_coffee_house(self, session, url, proxy):
        async with session.get(url, proxy=proxy) as response:
            if response.status != 200:
                print('Нет соеденения ', response.status)
                print('response.url', response.url)
                pass
            else:
                result_list = []
                result = {}
                html = await response.text()
                soup = bs(html, "lxml")

                name_coffee_shop = soup.find('h1')
                instagram = soup.find('p', class_='note')
                places = await self._places_info(soup)

                print(name_coffee_shop.text)
                print('\n')

                if name_coffee_shop is not None:
                    name_coffee_shop = name_coffee_shop.text
                if instagram is not None:
                    instagram = instagram.text

                result.update({
                    str(response.url): {
                        'name': name_coffee_shop,
                        'instagram': instagram,
                        'places': places,
                    }
                })
                result_list.append(result)
                await self._write_to_db(result_list)
                return result

    async def _client_session(self):
        url = url_site
        timeout = aiohttp.ClientTimeout(total=None, sock_connect=5, sock_read=5)
        headers = {"User-Agent": generate_user_agent()}
        proxy = FreeProxy(rand=True).get()
        try:
            async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=3, force_close=True, verify_ssl=False),
                                             headers=headers, timeout=timeout) as session:
                response = await session.get(url, proxy=proxy)
                html = await response.text()
                soup = bs(html, "lxml")
                coffee_house_names = soup.find_all('a', class_='shop-link')
                tasks = []

                for name in coffee_house_names:
                    task = asyncio.create_task(self._page_coffee_house(session, name.get('href'), proxy))
                    tasks.append(task)
                    # break

                await asyncio.gather(*tasks)
        except Exception as ex:
            print('ERROR', ex)
            ParsingSite()

    def _main(self):
        while True:
            hour = datetime.now().hour
            minute = datetime.now().minute
            if hour == 00 and minute == 00:
                try:
                    asyncio.run(create_db_backup())
                    asyncio.run(self._client_session())
                    asyncio.run(self._add_more_db())
                    break
                except asyncio.TimeoutError:
                    print("asyncio.TimeoutError")
