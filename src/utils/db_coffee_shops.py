import os
import sqlite3 as sl
from src.config import Config

config = Config()
NAME_DB = config.get_val('name_db')
NAME_DB_BACKUP = config.get_val('name_db_backup')


async def _connection():
    path = os.path.join(os.getcwd(), 'data', 'db')
    con = sl.connect(f'{path}/{NAME_DB}', isolation_level=None)
    return con


async def create_table():
    con = await _connection()
    with con:
        con.execute("""
            CREATE TABLE IF NOT EXISTS coffee_shops (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                title TEXT NOT NULL,
                inst_url TEXT,
                latitude REAL NOT NULL,
                longitude REAL NOT NULL,
                time_work TEXT,
                address TEXT NOT NULL,
                UNIQUE (latitude, longitude)
            );
        """)


async def write_new_shop(title, inst_url, latitude, longitude, time_work, address):
    con = await _connection()
    sql = 'INSERT OR IGNORE INTO coffee_shops (title, inst_url, latitude, longitude, time_work, address) VALUES (?, ?, ?, ?, ?, ?)'
    data = (title, inst_url, latitude, longitude, time_work, address)
    with con:
        con.execute(sql, data)
    return True


async def check_coffee_point(latitude, longitude):
    await create_table()
    con = await _connection()
    with con:
        sql = "SELECT * FROM coffee_shops WHERE latitude == ? AND longitude == ?"
        data = con.execute(sql, (latitude, longitude))
        for row in data:
            return row
    return False


async def get_elements():
    await create_table()
    con = await _connection()
    with con:
        sql = "SELECT * FROM coffee_shops ORDER BY id"
        data = con.execute(sql).fetchall()
        return data


async def get_point(point_id):
    await create_table()
    con = await _connection()
    with con:
        sql = 'SELECT * FROM coffee_shops WHERE id=?'
        data = con.execute(sql, (int(point_id),))
        return data


async def delete_coffee_point(point_id):
        con = await _connection()
        with con:
            sql = 'DELETE FROM coffee_shops WHERE id=?'
            con.execute(sql, (int(point_id),))
        return True


async def check_coffee_point_id(point_id):
    try:
        p_id = int(point_id)
    except:
        return False

    con = await _connection()
    with con:
        sql = "SELECT * FROM coffee_shops WHERE id == ?"
        data = con.execute(sql, (p_id,))
        for row in data:
            return row
    return False


async def search_coffee_point(text):
    def _str_lower(text_str): return str(text_str).lower()
    con = await _connection()
    con.create_function('mylower', 1, _str_lower)
    with con:
        search_text = f'%{text}%'.lower()
        sql = """
        SELECT * FROM coffee_shops WHERE mylower(title) LIKE ? ORDER BY title;
        """
        data = con.execute(sql, (search_text,)).fetchall()
        return data


async def create_db_backup():
    try:
        path = os.path.join(os.getcwd(), 'data', 'db_shops')
        db_file = f'{path}/{NAME_DB}'
        check_file = os.path.exists(db_file)
        if check_file:
            sqlite_con = sl.connect(db_file)
            backup_con = sl.connect(f'{path}/{NAME_DB_BACKUP}')
            with backup_con:
                sqlite_con.backup(backup_con)
    except sl.Error as error:
        print("Ошибка при резервном копировании: ", error)
    finally:
        if(backup_con):
            backup_con.close()
            sqlite_con.close()
