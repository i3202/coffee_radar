import urllib.parse


class Translations:

    start = 'Привет!\n' \
            'C помощью бота ты сможешь быстро найти ближайшие к тебе specialty кофейни ☕️🔥 \n\n' \
            'Просто отправь твоё местоположение боту и он покажет кофейни, которые находятся неподалёку от тебя\n\n' \
            '🔍 Для поиска конкретной кофейни - введи её название'

    def about_coffee_shop(self, name, instagram, distance, longitude, latitude, address, time_work):
        name_safe_string = urllib.parse.quote_plus(name)
        map_url = f'https://yandex.ru/maps/?ll={longitude},{latitude}&z=19&text={name_safe_string}'
        text = f'<b>{name}</b> \n\n' \
               f'📸 <a href="{instagram}">instagram</a> \n' \
               f'📏 <b>Расстояние:</b> {distance} км\n' \
               f'⏰ {time_work} \n' \
               f'📍 {address}'
        return text

    didnt_place = 'Среди вариантов нет подходящего места?'

    map_text = f'🗺 Показать на карте'

    welcome_admin = 'Чё надо?'
    add_new_point_title = 'Напиши название кофейни'
    add_new_point_inst = 'Напиши ссылку на инстаграм (или null, false, 0, none)'
    add_new_point_coordinates = 'Напиши координаты (широта, долгота)'
    add_new_point_work_schedule = 'Напиши график работы (ПН-ВС: 8:00-23:00) или null, false, 0, none'
    add_new_point_address = 'Напиши адрес'

    format_add_new_point = 'Напиши информацию о кофейне в формате:\n' \
                           'Название кофейни, ссылка на инст (не обязательно), ' \
                           'широта метоположения, долгота метоположения, ' \
                           'график работы (ПН-ВС: 8:00-23:00)(не обязательно), адрес. ' \
                           '\nВсё через запятую'

    del_point = 'Введи id кофейни'

    def output_coffee_points(self, name, instagram, longitude, latitude, address, point_id=None):
        if point_id:
            text = f'<b>id {point_id}</b> <b><a href="{instagram}">{name}</a></b> по адресу <code>{address}</code>'
        else:
            text = f'<b><a href="{instagram}">{name}</a></b> по адресу <code>{address}</code>'
        return text
