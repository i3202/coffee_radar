import os
import sqlite3 as sl
# from config import NAME_DB
from src.config import Config

config = Config()
NAME_DB = config.get_val('name_db')


async def _connection():
    path = os.path.join(os.getcwd(), 'data', 'db')
    con = sl.connect(f'{path}/{NAME_DB}', isolation_level=None)
    return con


async def create_table():
    con = await _connection()
    with con:
        con.execute("""
            CREATE TABLE IF NOT EXISTS user_info (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                user_id INTEGER NOT NULL,
                username TEXT,
                first_name TEXT,
                last_name TEXT,
                date_time TEXT NOT NULL,
                UNIQUE (user_id)
            );
        """)


async def write_user_info(user_id, username, first_name, last_name, date_time):
    con = await _connection()
    sql = 'INSERT OR IGNORE INTO user_info (user_id, username, first_name, last_name, date_time) VALUES (?, ?, ?, ?, ?)'
    data = (user_id, username, first_name, last_name, date_time)
    with con:
        con.execute(sql, data)
    return True


async def update_user_info(user_id, username, first_name, last_name, date_time):
    con = await _connection()
    sql = 'REPLACE INTO user_info (user_id, username, first_name, last_name, date_time) VALUES (?, ?, ?, ?, ?)'
    data = (user_id, username, first_name, last_name, date_time)
    with con:
        con.execute(sql, data)
    return True


async def check_user_id(user_id):
    try:
        u_id = int(user_id)
    except:
        return False

    con = await _connection()
    with con:
        sql = "SELECT * FROM user_info WHERE user_id == ?"
        data = con.execute(sql, (u_id,))
        for row in data:
            return row
    return False


async def get_users():
    await create_table()
    con = await _connection()
    with con:
        sql = "SELECT * FROM user_info ORDER BY id"
        data = con.execute(sql).fetchall()
        return data


async def w_u_user_info_db(message):
    user_id = message.chat.id
    username = message.chat.username
    first_name = message.chat.first_name
    last_name = message.chat.last_name
    date_time = message.date

    await create_table()
    check_user = await check_user_id(user_id)
    if check_user:
        await update_user_info(user_id, username, first_name, last_name, date_time)
    else:
        await write_user_info(user_id, username, first_name, last_name, date_time)

