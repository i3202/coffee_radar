from aiogram.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    KeyboardButton,
    ReplyKeyboardMarkup,
)
from aiogram.utils.callback_data import CallbackData


class Buttons:

    def send_location(self):
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn_location = KeyboardButton(text='📍 Отправить локацию', request_location=True)
        markup.add(btn_location)
        return markup

    def place(self, map_text, latitude, longitude):
        callback_data = CallbackData('send_location', 'latitude', 'longitude').new(latitude=latitude,
                                                                                   longitude=longitude)
        markup = InlineKeyboardMarkup()
        btn = InlineKeyboardButton(text=map_text, callback_data=callback_data)
        markup.add(btn)
        return markup

    def show_more(self):
        markup = InlineKeyboardMarkup(resize_keyboard=True)
        btn_show_more = InlineKeyboardButton(text='Показать ещё', callback_data='show_more')
        markup.add(btn_show_more)
        return markup

    def admin_btn(self):
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = KeyboardButton(text='Удалить кофейню')
        btn2 = KeyboardButton(text='Добавить кофейню')
        btn3 = KeyboardButton(text='Сделать бэкап')
        btn4 = KeyboardButton(text='На главную')
        btn5 = KeyboardButton(text='Статистика')
        markup.add(btn3, btn5)
        markup.add(btn1, btn2)
        markup.add(btn4)
        return markup

    def back(self):
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = KeyboardButton(text='На главную')
        btn2 = KeyboardButton(text='Назад')
        markup.add(btn1)
        markup.add(btn2)
        return markup

    def pages_points(self, number_page, all_pages):
        number_page_prev = number_page
        number_page_next = number_page+2

        if number_page == 0:
            number_page_prev = all_pages
        elif number_page == all_pages-1:
            number_page_next = 1

        markup = InlineKeyboardMarkup(resize_keyboard=True)
        btn_next = InlineKeyboardButton(text=f'{number_page_next}/{all_pages} →', callback_data='pages_points_next')
        btn_back = InlineKeyboardButton(text=f'← {number_page_prev}/{all_pages}', callback_data='pages_points_prev')
        markup.add(btn_back, btn_next)
        return markup

