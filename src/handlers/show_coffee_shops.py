import geopy.distance
from operator import itemgetter
from aiogram.dispatcher import FSMContext

from src.utils.translations import Translations
from src.utils.buttons import Buttons
from src.utils.db_coffee_shops import get_elements


class ShowCoffeeShops:
    def __init__(self):
        self.buttons = Buttons()
        self.text = Translations()

    async def write_location(self, key, value, state: FSMContext):
        async with state.proxy() as data:
            data[key] = value

    async def get_location(self, key, state: FSMContext):
        async with state.proxy() as data:
            return data.get(key)

    async def output_list_coffee_shop(self, message, state: FSMContext, show_next_coffee_shops=None):

        if show_next_coffee_shops is None:
            coffee_shops = await self.distance_to_coffee_shop(message, state, show_next_coffee_shops=None)
            if coffee_shops:
                coffee_shops = coffee_shops[:5]
        else:
            coffee_shops = await self.distance_to_coffee_shop(message, state, show_next_coffee_shops=True)
            if coffee_shops:
                coffee_shops = coffee_shops[-5:]

        if coffee_shops is None:
            await message.answer(text='Обнови своё местоположение')

        if coffee_shops:
            for i in coffee_shops:
                name = i.get('name')
                instagram = i.get('instagram')
                latitude = i.get('latitude')
                longitude = i.get('longitude')
                user_longitude = i.get('user_longitude')
                user_latitude = i.get('user_latitude')
                distance = i.get('distance')
                address = i.get('address')
                time_work = i.get('time_work')

                if distance is not None:
                    distance = round(distance, 2)
                map_text = self.text.map_text
                markup = self.buttons.place(map_text=map_text, latitude=latitude, longitude=longitude)
                main_text = self.text.about_coffee_shop(name, instagram, distance, longitude, latitude, address, time_work)
                await message.answer(text=main_text, parse_mode='HTML',
                                     disable_web_page_preview=True, reply_markup=markup)

        if show_next_coffee_shops is None:
            markup = self.buttons.show_more()
            await message.answer(text=self.text.didnt_place, parse_mode='HTML',
                                 disable_web_page_preview=True, reply_markup=markup)

    async def distance_to_coffee_shop(self, message, state: FSMContext, show_next_coffee_shops):
        if show_next_coffee_shops is None:
            await self.location(message, state)
            user_lat = await self.get_location('latitude', state)
            user_lng = await self.get_location('longitude', state)
        else:
            lat = await self.get_location('latitude', state)
            lng = await self.get_location('longitude', state)
            if lat or lng:
                user_lat = lat
                user_lng = lng
            else:
                await self.location(message, state)
                user_lat = await self.get_location('latitude', state)
                user_lng = await self.get_location('longitude', state)

        if user_lat or user_lng:
            list_places_sorted_by_distance = []
            get_el = await get_elements()

            for el in get_el:
                name = el[1]
                inst_url = el[2]
                latitude = el[3]
                longitude = el[4]
                time_work = el[5]
                address = el[6]
                user_coords = (user_lat, user_lng)
                shop_coords = (latitude, longitude)
                distance = geopy.distance.geodesic(user_coords, shop_coords).km
                places_sorted_by_distance = {
                    'name': name,
                    'instagram': inst_url,
                    'latitude': latitude,
                    'longitude': longitude,
                    'user_latitude': user_lat,
                    'user_longitude': user_lng,
                    'distance': distance,
                    'address': address,
                    'time_work': time_work,
                }
                list_places_sorted_by_distance.append(places_sorted_by_distance)
            places_sorted_by_distance = sorted(list_places_sorted_by_distance, key=itemgetter('distance'))
            return places_sorted_by_distance[:10]

    async def location(self, message, state: FSMContext):
        try:
            message = message
        except:
            message = message.edited_message

        if message.location:
            lat = message.location.latitude
            lng = message.location.longitude
            await self.write_location('latitude', lat, state)
            await self.write_location('longitude', lng, state)
