import math
from aiogram.dispatcher import FSMContext

from src.utils.db_coffee_shops import search_coffee_point
from src.utils.translations import Translations
from src.utils.buttons import Buttons
from src.config import Config

config = Config()
id_admin = config.get_val('id_admin')


class Search:

    def __init__(self):
        self.text = Translations()
        self.buttons = Buttons()
        self._count_el = 10

    async def output_point(self, text, message, state: FSMContext):
        show_point_func = await self.show_point(text, message, state)
        if show_point_func is not True:
            return show_point_func

        return_text = ''
        async with state.proxy() as data:
            if data.get('result_list'):
                show_point = data['result_list']
                number_page = data['number_page']
        len_show_point = len(show_point)

        if len_show_point < self._count_el:
            for sp in show_point:
                return_text += sp + '\n'
            return await message.answer(text=return_text, parse_mode='HTML', disable_web_page_preview=True)
        else:
            page_count = math.ceil(len_show_point / self._count_el)
            buttons = self.buttons.pages_points(number_page, page_count)

            for sp in show_point[0:self._count_el]:
                return_text += sp + '\n'

            return await message.answer(text=return_text, parse_mode='HTML',
                                        disable_web_page_preview=True, reply_markup=buttons)

    async def update_output_point(self, message, show_point, pages_count, state: FSMContext):
        return_text = ''
        async with state.proxy() as data:
            number_page = data['number_page']

        for sp in show_point:
            return_text += sp + '\n'

        buttons = self.buttons.pages_points(number_page, pages_count)
        return await message.edit_text(text=return_text, parse_mode='HTML',
                                       disable_web_page_preview=True, reply_markup=buttons)

    async def show_point(self, text, message, state: FSMContext):
        user_id = message.chat.id
        if len(text) >= 3:
            result_list = []
            coffee_points = await search_coffee_point(text)
            for cp in coffee_points:
                point_id = cp[0]
                name = cp[1]
                inst_url = cp[2]
                latitude = cp[3]
                longitude = cp[4]
                time_work = cp[5]
                address = cp[6]
                if str(id_admin) == str(user_id):
                    return_text = self.text.output_coffee_points(name, inst_url, longitude, latitude, address, point_id)
                else:
                    return_text = self.text.output_coffee_points(name, inst_url, longitude, latitude, address)
                result_list.append(return_text)

            if result_list:
                async with state.proxy() as data:
                    data['result_list'] = result_list
                    data['number_page'] = 0
                return True
            else:
                return await message.answer(text='Ничего не найдено')
        else:
            return await message.answer(text='Нужно минимум ввести 3 симовла для поиска')

    async def _list_in_list(self, state: FSMContext):
        async with state.proxy() as data:
            show_point = data['result_list']

        len_list = len(show_point)
        coffee_points = [show_point[i:i+self._count_el] for i in range(0, len_list, self._count_el)]
        return coffee_points

    async def search_next(self, message, state: FSMContext):
        coffee_points = await self._list_in_list(state)
        async with state.proxy() as data:
            show_point = data['result_list']
            pages_count = math.ceil(len(show_point) / self._count_el)

            if data['number_page'] < pages_count-1:
                data['number_page'] += 1
            else:
                data['number_page'] = 0
            number_page = data['number_page']
            coffee_points_page = coffee_points[number_page]
        return await self.update_output_point(message, coffee_points_page, pages_count, state)

    async def search_prev(self, message, state: FSMContext):
        coffee_points = await self._list_in_list(state)
        async with state.proxy() as data:
            show_point = data['result_list']
            pages_count = math.ceil(len(show_point) / self._count_el)

            if data['number_page'] == 0:
                data['number_page'] = pages_count-1
            else:
                data['number_page'] -= 1

            number_page = data['number_page']
            coffee_points_page = coffee_points[number_page]
        return await self.update_output_point(message, coffee_points_page, pages_count, state)
