import asyncio
from aiogram import Bot
from aiogram import types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from datetime import datetime

from src.config import Config
from src.utils.translations import Translations
from src.utils.buttons import Buttons
from src.utils.db_user_info import w_u_user_info_db
from src.handlers.show_coffee_shops import ShowCoffeeShops
from src.admin.func_admin import Admin, AddPoint, DelPoint
from src.handlers.search_points import Search

config = Config()
token = config.get_val('token')
id_admin = config.get_val('id_admin')


class Telegram:

    def __init__(self):
        self.bot = Bot(token=token)
        self.dp = Dispatcher(self.bot, storage=MemoryStorage())
        self.buttons = Buttons()
        self.text = Translations()
        self.show_coffee_shops = ShowCoffeeShops()
        self.admin = Admin()
        self.search = Search()
        self.admin.register_handlers_admin(self.dp)

        @self.dp.message_handler(commands=['start'])
        async def _on_start(message: types.Message, state: FSMContext):
            await w_u_user_info_db(message)
            await state.finish()
            markup = self.buttons.send_location()
            await message.answer(text=self.text.start, reply_markup=markup)

        @self.dp.message_handler(commands=['admin'])
        async def admin(message: types.Message, state: FSMContext):
            user_id = message.chat.id
            if user_id == int(id_admin):
                await state.finish()
                markup = self.buttons.admin_btn()
                await message.answer(text=self.text.welcome_admin, reply_markup=markup)

        @self.dp.message_handler(content_types=['text'])
        async def message_handler(message: types.Message, state: FSMContext):
            await w_u_user_info_db(message)
            user_id = message.chat.id
            text = message.text

            if user_id == int(id_admin):
                match text.lower():
                    case 'сделать бэкап':
                        return await self.admin.make_backup(self.bot)
                    case 'статистика':
                        return await self.admin.get_statistics(message)
                    case 'добавить кофейню':
                        await AddPoint.add_new_point_title.set()
                        return await self.bot.send_message(chat_id=id_admin, text=self.text.add_new_point_title,
                                                           reply_markup=self.buttons.back())
                    case 'удалить кофейню':
                        await DelPoint.del_point.set()
                        return await self.bot.send_message(chat_id=id_admin, text=self.text.del_point)
                    case 'на главную':
                        await state.finish()
                        markup = self.buttons.send_location()
                        return await message.answer(text=self.text.start, reply_markup=markup)
            await state.finish()
            return await self.search.output_point(text, message, state)

        @self.dp.message_handler(content_types=['location'])
        async def location(message: types.Message, state: FSMContext):
            await w_u_user_info_db(message)
            return await self.show_coffee_shops.output_list_coffee_shop(message, state, show_next_coffee_shops=None)

        @self.dp.callback_query_handler(lambda call: True)
        async def callback_inline(call: types.CallbackQuery, state: FSMContext):
            try:
                await self.bot.answer_callback_query(call.id)
                user_id = call.message.chat.id
                message_id = call.message.message_id

                call_data_val = call.data.split(":")
                match call_data_val:
                    case 'send_location', latitude, longitude:
                        await self.bot.send_location(chat_id=user_id, latitude=latitude, longitude=longitude,
                                                     horizontal_accuracy=10.0, reply_to_message_id=message_id,
                                                     allow_sending_without_reply=True)

                match call.data:
                    case 'show_more':
                        await self.bot.delete_message(chat_id=user_id, message_id=message_id)
                        await self.show_coffee_shops.output_list_coffee_shop(call.message, state, show_next_coffee_shops=True)
                        await state.finish()
                    case 'pages_points_prev':
                        await self.search.search_prev(call.message, state)
                    case 'pages_points_next':
                        await self.search.search_next(call.message, state)
            except Exception as ex:
                print(f'ERROR {ex}')

        self.main()

    def main(self):
        print('...Бот работает...')
        print('\n')
        try:
            async def on_startup(_):
                asyncio.create_task(self._send_backup())

            executor.start_polling(self.dp, skip_updates=True)
        except Exception as ex:
            print('ERRORS', ex)

    async def _send_backup(self):
        while True:
            hour = datetime.now().hour
            minute = datetime.now().minute
            if hour == 23 and minute == 00:
                try:
                    await Admin().make_backup(self.bot)
                    break
                except asyncio.TimeoutError:
                    print("asyncio.TimeoutError")

