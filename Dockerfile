FROM python:3.10
COPY . .
RUN mkdir -p $PWD/data/db
RUN pip install -r requirements.txt